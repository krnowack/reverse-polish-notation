import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(JUnitParamsRunner.class)
public class ReversePolishNotationTest {

    @Test
    @Parameters({
            "8 2 +, 10",
            "8 2 -, 6",
            "8 2 *, 16",
            "8 2 /, 4"
    })
    public void shouldCalculateSingleMinutesRow(String rpnRepresentation, String expectedOperationResult) {
        //Given
        ReversePolishNotation rpn = new ReversePolishNotationImplementation();
        //When
        long result = rpn.calculate(rpnRepresentation);
        //Then
        assertThat(result).isEqualTo(expectedOperationResult);
    }
}