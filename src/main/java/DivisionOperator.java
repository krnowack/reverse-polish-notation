
class DivisionOperator implements IOperator {
    @Override
    public long performOperation(long a, long b) {
        return a / b;
    }
}