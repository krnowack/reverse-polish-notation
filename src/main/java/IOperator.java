
interface IOperator
{
    public long performOperation(long a, long b);
};